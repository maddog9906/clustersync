# Cluster Sync

## What is cluster-sync
`cluster-sync` is a library to ensure that only one node is master with multiple slaves, and automatic failover.
It currently supports 2 mechanism,
* JDBC and
* REDIS

## Declaring the dependency
```Groovy
    repositories {
        ...
        maven { url 'https://raw.githubusercontent.com/alvin9906/maven-repo/master' }  // maven-repo at github
        ...
    }    
     
   dependencies {
        .....
        compile 'com.battletech.maddog:cluster-sync:0.3.2-spring-boot-2.1.6' 
        .....
   }
```

## Usage
You will need to import the bean configuration:

For JDBC mode,

```Groovy
@Configuration
@EnableMasterSlaveSync
class AppConfig {
}
```

For REDIS mode,

```Groovy
@Configuration
@EnableRedisMasterSlaveSync
class AppConfig {
}
```

After that, you will just need to autowire and use the masterSlaveService as a service/component.
This bean implement the following interface

```Groovy
    boolean isMasterNode() // Return true if the node us current the MASTER

    String getNodeId() // A nodeId. Dynamic value assigned.

    String getNodeName() // Nodename. This will be the machine's hostname.
```

To do something when it is a master node, do this:
```Groovy
@Autowired
MasterSlaveService masterSlaveService

def someMethod() {
    if (masterSlaveService.isMaster()) {
      // Do something....
    }
}

```



## How it works
### JDBC Mechanism
A table will be created to synchronised the locking.

```Groovy
CREATE TABLE `cluster_sync` (
  `cluster_name` varchar(255) NOT NULL,
  `node_id` varchar(255) DEFAULT NULL,
  `node_name` varchar(255) DEFAULT NULL,
  `last_updated` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`cluster_name`)
)
```

Upon start up, it will create the above table if it is not in the defined schema. By default, if no schema is provided, it will be created in the default schema.

This make uses of locking the corresponding row to determine which node has the lock.
The cluster name can be configured but is defaulted to `default-app`

This bean will query the database every 2 seconds.
It will update the `last_updated` field if it is the MASTER.
If it is a SLAVE, it will take over as MASTER if the current MASTER had not update the `last_updated` field for more than 10secs.

You may need to configure your application property file if u wish to change the settings:
```Groovy
clustering.settings:
  appName:  default-app      # Default to `default-app`
  schemaName:                # Default to null

```

### REDIS Mechanism
This will make use of Redis for synchronisation. 

The application is expecting a bean `StringRedisTemplate` for it to work.
The key is `LOCK_xxxxx` where `xxxxx` is the appName.

At startup, it would tries to set a KV pair if the key is absent. The expiry is set at 10sec.

Every 2 seconds, a slave node would attempts to set the key. 
If successful, the node will be promoted as `master`.

For the same interval, the `master` node would attempt to retrieve the value based on the key. 
If the value indicates that the current master is itself, 
it would set back the same value and extend the expiry by 10secs. 
If the value indicates that the master is some other node, 
it would demoted itself as a slave node.

For redis mode, the only configuration necessary is the appName.

```Groovy
clustering.settings:
  appName:  default-app      # Default to `default-app`
```

