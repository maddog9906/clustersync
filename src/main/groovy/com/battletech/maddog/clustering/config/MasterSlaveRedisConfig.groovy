package com.battletech.maddog.clustering.config

import com.battletech.maddog.clustering.service.MasterSlaveRedisService
import com.battletech.maddog.clustering.service.MasterSlaveService
import com.battletech.maddog.clustering.service.MasterSlaveTimerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.core.StringRedisTemplate

@Configuration
class MasterSlaveRedisConfig {

    @Value('${clustering.settings.appName:default-app}')
    String appName

    @Autowired
    StringRedisTemplate stringRedisTemplate

    @Bean
    MasterSlaveService masterSlaveService() {
        new MasterSlaveRedisService(stringRedisTemplate, appName)
    }

    @Bean
    MasterSlaveTimerService masterSlaveTimerService(){
        new MasterSlaveTimerService(masterSlaveService())
    }
}

