package com.battletech.maddog.clustering.config

import com.battletech.maddog.clustering.service.MasterSlaveService
import com.battletech.maddog.clustering.service.MasterSlaveJdbcService
import com.battletech.maddog.clustering.service.MasterSlaveTimerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.transaction.PlatformTransactionManager

import javax.sql.DataSource

@Configuration
class MasterSlaveConfig {

    @Value('${clustering.settings.appName:default-app}')
    String appName

    @Value('${clustering.settings.schemaName:#{NULL}}')
    String schemaName

    @Autowired
    DataSource dataSource

    @Bean
    MasterSlaveService masterSlaveService() {
        new MasterSlaveJdbcService(groovySqlTxnManager(), dataSource, schemaName, appName)
    }

    @Bean
    MasterSlaveTimerService masterSlaveTimerService(){
        new MasterSlaveTimerService(masterSlaveService())
    }

    private PlatformTransactionManager groovySqlTxnManager() {
        return new DataSourceTransactionManager(dataSource)
    }

}
