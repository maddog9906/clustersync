package com.battletech.maddog.clustering.service

import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean

class MasterSlaveTimerService implements InitializingBean, DisposableBean {

    MasterSlaveService masterSlaveService

    MasterSlaveTimerService(MasterSlaveService masterSlaveService){
        this.masterSlaveService = masterSlaveService
    }

    @Override
    void afterPropertiesSet() throws Exception {
        masterSlaveService.init()
        new Timer().schedule(new SynchroniseNodesTimerTask(), 2000, 2000)
    }

    @Override
    void destroy() throws Exception {
        masterSlaveService.shutDown()
    }

    class SynchroniseNodesTimerTask extends TimerTask {
        @Override
        void run() {
            masterSlaveService.synchroniseLock()
        }
    }
}
