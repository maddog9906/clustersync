package com.battletech.maddog.clustering.service

import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.support.DefaultTransactionDefinition

import javax.sql.DataSource
import java.sql.SQLIntegrityConstraintViolationException
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.concurrent.atomic.AtomicBoolean

@Slf4j
class MasterSlaveJdbcService implements MasterSlaveService {
    AtomicBoolean hasLock = new AtomicBoolean(false)
    PlatformTransactionManager transactionManager = null
    Sql groovySql = null
    String uniqueNodeId = null
    String nodeName = null
    String appName = null
    String tableName = null

    String GET_RECORD_SQL
    String GET_AND_LOCK_RECORD_SQL
    String UPDATE_LOCK_SQL

    MasterSlaveJdbcService(PlatformTransactionManager transactionManager,
                           DataSource dataSource, String schema, String appName) {
        this.transactionManager = transactionManager
        this.groovySql = new Sql(dataSource)
        this.uniqueNodeId = UUID.randomUUID().toString()
        this.nodeName = InetAddress.getLocalHost().getHostName()
        this.appName = appName

        this.tableName = schema ? (schema + ".cluster_sync") : "cluster_sync"

        GET_RECORD_SQL = """
                           SELECT * FROM $tableName WHERE cluster_name  = '$appName'
                          """

        GET_AND_LOCK_RECORD_SQL = """
                                   SELECT * FROM $tableName WHERE cluster_name  = '$appName' FOR UPDATE
                                   """

        UPDATE_LOCK_SQL = """
                              UPDATE $tableName
                                 SET node_id = :uniqueNodeId, node_name = :nodeName, last_updated = :now 
                              WHERE
                                 cluster_name = '$appName'
                              """
    }

    @Override
    boolean isMasterNode() {
        hasLock.get()
    }

    @Override
    String getNodeId() {
        uniqueNodeId
    }

    @Override
    String getNodeName() {
        nodeName
    }

    @Override
    void init() {
        final String CREATE_TABLE = """
                                        CREATE TABLE IF NOT EXISTS $tableName (
                                             cluster_name varchar(255) NOT NULL,
                                             node_id varchar(255) DEFAULT NULL,
                                             node_name varchar(255) DEFAULT NULL,
                                             last_updated datetime(3) DEFAULT NULL,
                                           PRIMARY KEY (cluster_name)
                                        )
                                     """
        final String INSERT_APP_NAME = """
                                        INSERT INTO $tableName (cluster_name) VALUES ('$appName') 
                                        """

        // Ensure table is there
        doTransactionalJob({
            groovySql.execute(CREATE_TABLE)
        }, "masterSlaveServiceInit", Propagation.REQUIRES_NEW)

        // Ensure record is there
        doTransactionalJob({
            def existingRecord = getClusterRecord(false)
            if (!existingRecord) {
                try {
                    groovySql.execute(INSERT_APP_NAME)
                } catch (SQLIntegrityConstraintViolationException e) {
                    // we are ignoring this error
                    log.warn("Unable to insert record in cluster sync table. This is probably harmless as it indicates the record is already there.")
                }
                catch (Exception e) {
                    throw e
                }
            }
        }, "masterSlaveServiceInit", Propagation.REQUIRES_NEW)

    }

    @Override
    void shutDown() throws Exception {
        doTransactionalJob({
            if (groovySql) {
                try {
                    if (hasLock.get()) {
                        resetClusterRecord()
                    }
                } finally {
                    try {
                        groovySql.close()
                    } catch (Exception e) {
                    }
                }
            }
        }, "masterSlaveServiceShutdown", Propagation.REQUIRES_NEW)
    }

    @Override
    void synchroniseLock() {
        doTransactionalJob({
            doSynchroniseLock()
        }, "masterSlaveServiceSyncLock", Propagation.REQUIRES_NEW)
    }

    private void doSynchroniseLock() {
        def lockRecord = getClusterRecord()
        // If this node is SLAVE
        if (!hasLock.get()) {
            def lastUpdated = lockRecord.get("last_updated") ?
                    (lockRecord.get("last_updated") as Date).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime() : null
            // In init state, `node_id` and `last_updated` may be null
            // If that's the case, or if the `last_updated` was not updated for >10secs,
            // we would want to take over as MASTER node...
            if (!lockRecord.'node_id' || !lastUpdated ||
                    ChronoUnit.SECONDS.between(lastUpdated, LocalDateTime.now()) > 10) {

                if (attemptToGetLock(lockRecord)) {
                    hasLock.set(true)
                    log.info("GetLock successful [JDBC]. $nodeName [$uniqueNodeId] is promoted to MASTER for $appName cluster")
                } else {
                    log.info("GetLock fail [JDBC]. $nodeName [$uniqueNodeId] remains as SLAVE for $appName cluster")
                }
            }
        }
        // If this node is MASTER
        else {
            if ((lockRecord.'node_id' != uniqueNodeId) ||
                    (!attemptToGetLock(lockRecord))) {
                hasLock.set(false)
                log.warn("$nodeName [$uniqueNodeId] was removed as MASTER for $appName cluster. Now operating as SLAVE.")
            }
        }
    }

    private boolean attemptToGetLock(def lastKnownRecord) {
        boolean response = false
        try {
            def currentRecord = getClusterRecord(true)
            // We use SELECT FOR UPDATE... Since mysql does not support SELECT FOR UPDATE NO WAIT,
            // there's a chance that a lock might be return...
            // We know that when we attempt to get a lock, the process will update the last_updated field
            // So, after we get the lock, if the last_updated was different from the previous record,
            // we know that some other process have already updated that record, so we should not attempt to declare
            // as MASTER
            if (lastKnownRecord.'last_updated' != currentRecord.'last_updated') {
                // ok some other node had updated this... so this node cannot be master...
                response = false
            } else {
                updateClusterRecord()
                response = true
            }
        } catch (Exception e) {
            response = false
        }

        response
    }

    private getClusterRecord(boolean needLock = false) {
        if (needLock) {
            groovySql.firstRow GET_AND_LOCK_RECORD_SQL
        } else {
            groovySql.firstRow GET_RECORD_SQL
        }
    }

    private updateClusterRecord() {
        groovySql.executeUpdate([uniqueNodeId: uniqueNodeId, nodeName: nodeName, now: new Date()], UPDATE_LOCK_SQL)
    }


    private resetClusterRecord() {
        groovySql.executeUpdate([uniqueNodeId: null, nodeName: null, now: null], UPDATE_LOCK_SQL)
    }

    private void doTransactionalJob(
                                Runnable runnable,
                                String txnName,
                                Propagation propagation=Propagation.REQUIRED,
                                boolean readOnly=false) {
        DefaultTransactionDefinition txn = new DefaultTransactionDefinition(
                name: txnName, readOnly: readOnly,
                propagationBehavior: propagation.value(),
                isolationLevel: Isolation.DEFAULT.value()
        )
        TransactionStatus status = transactionManager.getTransaction(txn)
        try {
            runnable.run()
        }
        catch (Exception ex) {
            transactionManager.rollback(status)
            throw ex;
        }
        transactionManager.commit(status)
    }
}
