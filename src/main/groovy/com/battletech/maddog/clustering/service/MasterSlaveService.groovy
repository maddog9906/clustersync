package com.battletech.maddog.clustering.service

interface MasterSlaveService {

    boolean isMasterNode()

    String getNodeId()

    String getNodeName()

    void synchroniseLock()

    void init()

    void shutDown()

}