package com.battletech.maddog.clustering.service

import groovy.util.logging.Slf4j
import org.springframework.data.redis.core.StringRedisTemplate

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

@Slf4j
class MasterSlaveRedisService implements  MasterSlaveService {

    AtomicBoolean hasLock = new AtomicBoolean(false)
    String uniqueNodeId = null
    String nodeName = null
    String appName = null
    StringRedisTemplate redisTemplate = null

    private long lockExpiry = 10000L

    MasterSlaveRedisService(StringRedisTemplate redisTemplate, String appName) {
        this.appName = appName
        this.redisTemplate = redisTemplate
        this.nodeName = InetAddress.getLocalHost().getHostName()
        this.uniqueNodeId = UUID.randomUUID().toString()
    }

    @Override
    boolean isMasterNode() {
        hasLock.get()
    }

    @Override
    String getNodeId() {
        uniqueNodeId
    }

    @Override
    String getNodeName() {
        nodeName
    }

    @Override
    void init() {
        // do nothing...
    }

    @Override
    void shutDown() {
        // do nothing
    }

    @Override
    void synchroniseLock() {
        String lockKey = "LOCK_${appName}" as String
        String lockVal = "${nodeName}_${uniqueNodeId}" as String

        // If this node is SLAVE
        if (!hasLock.get()) {
            // For slave nodes, it will keep on trying to get lock...
            Boolean lock = redisTemplate.opsForValue().setIfAbsent(lockKey, lockVal)
            if (lock) {
                // set expiry
                redisTemplate.expire(lockKey, lockExpiry, TimeUnit.MILLISECONDS)
                hasLock.set(true)
                log.info("GetLock successful [REDIS]. $nodeName [$uniqueNodeId] is promoted to MASTER for $appName cluster")
            } else {
                log.info("GetLock fail [REDIS]. $nodeName [$uniqueNodeId] remains as SLAVE for $appName cluster")
            }
        }
        // If this node is MASTER
        else {
            // Find the lock...
            String val = redisTemplate.opsForValue().get(lockKey)
            if (val == lockVal) {
                // extend the lock expiry
                redisTemplate.expire(lockKey, lockExpiry, TimeUnit.MILLISECONDS)
            } else {
                hasLock.set(false)
                log.warn("$nodeName [$uniqueNodeId] was removed as MASTER for $appName cluster. Now operating as SLAVE.")
            }
        }
    }
}
