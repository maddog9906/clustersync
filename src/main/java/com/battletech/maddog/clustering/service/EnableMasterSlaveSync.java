package com.battletech.maddog.clustering.service;

import com.battletech.maddog.clustering.config.MasterSlaveConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({MasterSlaveConfig.class})
public @interface EnableMasterSlaveSync {
}
