package com.battletech.maddog.clustering

import org.hsqldb.Server
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.RedisStandaloneConfiguration
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy
import org.springframework.transaction.TransactionDefinition
import redis.embedded.RedisServer

import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import javax.sql.DataSource

@SpringBootApplication(
        scanBasePackages =["com.battletech.maddog.clustering.service"],
        exclude = [RedisAutoConfiguration])
class TestApp {

    Server dbServer
    RedisServer redisServer
    int redisPort = new Random().nextInt((16379 - 10000) + 1) + 10000

    static void main(String[] args) {
        SpringApplication.run(TestApp.class, args)
    }

    @PostConstruct
    void init() {
        dbServer = new Server()
        dbServer.setDatabaseName(0, 'master_slave_test')
        dbServer.setDatabasePath(0, './build/test-db/master_slave_test')
        dbServer.start()

        redisServer = new RedisServer(redisPort)
        redisServer.start()
    }

    @PreDestroy
    void shutDown() {
        dbServer.shutdown()
        redisServer.stop()
    }

    @Bean
    StringRedisTemplate stringRedisTemplate() {
        RedisStandaloneConfiguration configuration =
                new RedisStandaloneConfiguration(hostName: "localhost", port: redisPort)

       new StringRedisTemplate(new JedisConnectionFactory(configuration))
    }

    @Bean
    DataSource getDataSource() {
        new TransactionAwareDataSourceProxy (
            new org.apache.tomcat.jdbc.pool.DataSource (
                    url: 'jdbc:hsqldb:hsql://localhost/master_slave_test',
                    driverClassName: 'org.hsqldb.jdbc.JDBCDriver',
                    username: 'SA',
                    password: '',
                    maxActive: 10,
                    maxAge: 600000,
                    maxIdle: 5,
                    maxWait: 10000,
                    minIdle: 1,
                    initialSize: 1,
                    validationQuery: 'select 1 from information_schema.system_users',
                    testOnBorrow: true,
                    testOnReturn: true,
                    testWhileIdle: true,
                    defaultTransactionIsolation: TransactionDefinition.ISOLATION_READ_COMMITTED,
                    minEvictableIdleTimeMillis: 60000,
                    timeBetweenEvictionRunsMillis: 5000
            )
        )
    }
}
