package com.battletech.maddog.clustering.service

import groovy.sql.Sql
import org.springframework.transaction.PlatformTransactionManager
import spock.lang.Specification
import spock.lang.Subject
import spock.util.mop.ConfineMetaClassChanges

import javax.sql.DataSource

class MasterSlaveJdbcServiceTest extends Specification {

    @Subject
    MasterSlaveJdbcService masterSlaveService

    PlatformTransactionManager mockTxnManager = Mock()
    DataSource mockDataSource = Mock()
    Sql mockGroovySql = Mock(Sql, constructorArgs: [mockDataSource])

    final String SCHEMA_NAME = 'SCHEMA'
    final String APP_NAME = 'TEST_APP'

    final String MY_UUID = 'b3cc24ec-ba1b-45b7-8b83-bc53a5ee9c85'
    final String MY_NODENAME = 'MY-NODE-NAME'

    final String OTHER_UUID = '99999999-9999-9999-9999-999999999999'
    final String OTHER_NODENAME = 'OTHER-NODE-NAME'

    final Date NOW = new Date()
    final Date TIMEOUT = new Date(System.currentTimeMillis() - 60_000)

    def setup() {
        masterSlaveService = new MasterSlaveJdbcService(mockTxnManager, mockDataSource, SCHEMA_NAME, APP_NAME)
        masterSlaveService.groovySql = mockGroovySql
        masterSlaveService.uniqueNodeId = MY_UUID
        masterSlaveService.nodeName = MY_NODENAME
    }

    @ConfineMetaClassChanges([Date])
    def "become master, then shutdown removing the lock"() {
        given:
        Date.metaClass.constructor = { return NOW.clone() }
        masterSlaveService.init()

        when:
        masterSlaveService.synchroniseLock()

        then:
        1 * mockTxnManager.getTransaction(*_)
        1 * mockGroovySql.firstRow(masterSlaveService.GET_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : null,
                node_name   : null,
                last_updated: null
        ]
        1 * mockGroovySql.firstRow(masterSlaveService.GET_AND_LOCK_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : null,
                node_name   : null,
                last_updated: null
        ]
        1 * mockGroovySql.executeUpdate([uniqueNodeId: MY_UUID, nodeName: MY_NODENAME, now: NOW], masterSlaveService.UPDATE_LOCK_SQL)
        assert masterSlaveService.isMasterNode()
        1 * mockTxnManager.commit(*_)
        0 * _

        when:
        masterSlaveService.shutDown()

        then:
        1 * mockTxnManager.getTransaction(*_)
        1 * mockGroovySql.executeUpdate([uniqueNodeId: null, nodeName: null, now: null], masterSlaveService.UPDATE_LOCK_SQL)
        1 * mockGroovySql.close()
        1 * mockTxnManager.commit(*_)
        0 * _
    }

    def "don't become master, then shutdown leaving lock untouched"() {
        given:
        masterSlaveService.init()

        when:
        masterSlaveService.synchroniseLock()

        then:
        1 * mockTxnManager.getTransaction(*_)
        1 * mockGroovySql.firstRow(masterSlaveService.GET_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : OTHER_UUID,
                node_name   : OTHER_NODENAME,
                last_updated: NOW
        ]
        0 * mockGroovySql.executeUpdate(_, masterSlaveService.UPDATE_LOCK_SQL)
        assert !masterSlaveService.isMasterNode()
        1 * mockTxnManager.commit(*_)
        0 * _

        when:
        masterSlaveService.shutDown()

        then:
        1 * mockTxnManager.getTransaction(*_)
        0 * mockGroovySql.executeUpdate(_, masterSlaveService.UPDATE_LOCK_SQL)
        1 * mockGroovySql.close()
        1 * mockTxnManager.commit(*_)
        0 * _
    }

    @ConfineMetaClassChanges([Date])
    def "if lock is stale, become master"() {
        given:
        Date.metaClass.constructor = { return NOW.clone() }
        masterSlaveService.init()

        when:
        masterSlaveService.synchroniseLock()

        then:
        1 * mockTxnManager.getTransaction(*_)
        1 * mockGroovySql.firstRow(masterSlaveService.GET_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : OTHER_UUID,
                node_name   : OTHER_NODENAME,
                last_updated: TIMEOUT
        ]
        1 * mockGroovySql.firstRow(masterSlaveService.GET_AND_LOCK_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : OTHER_UUID,
                node_name   : OTHER_NODENAME,
                last_updated: TIMEOUT
        ]
        1 * mockGroovySql.executeUpdate([uniqueNodeId: MY_UUID, nodeName: MY_NODENAME, now: NOW], masterSlaveService.UPDATE_LOCK_SQL)
        assert masterSlaveService.isMasterNode()
        1 * mockTxnManager.commit(*_)
        0 * _
    }

    @ConfineMetaClassChanges([Date])
    def "if lock is stale, but locking row fails, stay as slave"() {
        given:
        Date.metaClass.constructor = { return NOW.clone() }
        masterSlaveService.init()

        when:
        masterSlaveService.synchroniseLock()

        then:
        1 * mockTxnManager.getTransaction(*_)
        1 * mockGroovySql.firstRow(masterSlaveService.GET_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : OTHER_UUID,
                node_name   : OTHER_NODENAME,
                last_updated: TIMEOUT
        ]
        1 * mockGroovySql.firstRow(masterSlaveService.GET_AND_LOCK_RECORD_SQL) >> [
                cluster_name: APP_NAME,
                node_id     : OTHER_UUID,
                node_name   : OTHER_NODENAME,
                last_updated: NOW
        ]
        0 * mockGroovySql.executeUpdate([uniqueNodeId: MY_UUID, nodeName: MY_NODENAME, now: NOW], masterSlaveService.UPDATE_LOCK_SQL)
        assert !masterSlaveService.isMasterNode()
        1 * mockTxnManager.commit(*_)
        0 * _
    }

}
