package com.battletech.maddog.clustering.service

import com.battletech.maddog.clustering.TestApp
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification
import spock.lang.Subject

@SpringBootTest(classes = TestApp, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@EnableRedisMasterSlaveSync
@ActiveProfiles("test")
class MasterSlaveRedisServiceITest extends Specification {

    @Autowired
    @Subject
    MasterSlaveService masterSlaveService

    def "masterSlaveService - can successfully be MASTER"() {
        when:
        Thread.sleep(5_000)
        boolean result = masterSlaveService.isMasterNode()

        then:
        assert result
    }
}
